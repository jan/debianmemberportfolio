# -*- python -*-
# -*- coding: utf-8 -*-
#
# Debian Member Portfolio Service views
#
# Copyright © 2015-2023 Jan Dittberner <jan@dittberner.info>
#
# This file is part of the Debian Member Portfolio Service.
#
# Debian Member Portfolio Service is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Debian Member Portfolio Service is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
import json
import logging

from config import LANGUAGES
from debianmemberportfolio import app, babel
from flask import abort, g, make_response, render_template, request

# noinspection PyPep8Naming
from flask_babel import lazy_gettext as N_

from .forms import DeveloperData, DeveloperDataRequest
from .model import dddatabuilder
from .model.urlbuilder import build_urls

log = logging.getLogger(__name__)

#: This dictionary defines groups of labeled portfolio items.
_LABELS = {
    "overview": {
        "label": N_("Overview"),
        "ddpo": N_("Debian Member's Package Overview"),
        "alladdresses": N_(
            """Debian Member's Package Overview
... showing all email addresses"""
        ),
    },
    "bugs": {
        "label": N_("Bugs"),
        "received": N_(
            """bugs received
(note: co-maintainers not listed, see \
<a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?\
bug=430986">#430986</a>)"""
        ),
        "reported": N_("bugs reported"),
        "usertags": N_("user tags"),
        "wnpp": N_('<a href="https://wiki.debian.org/WNPP">WNPP</a>'),
        "correspondent": N_("correspondent for bugs"),
        "graph": N_("one year open bug history graph"),
    },
    "build": {
        "label": N_("Build"),
        "buildd": N_("buildd.d.o"),
        "igloo": N_("igloo"),
    },
    "qa": {
        "label": N_("Quality Assurance"),
        "dmd": N_("maintainer dashboard"),
        "lintian": N_("lintian reports"),
        "lintianfull": N_(
            'full lintian reports (i.e. including \
"info"-level messages)'
        ),
        "piuparts": N_("piuparts"),
        "janitor": N_("Debian Janitor"),
    },
    "lists": {
        "label": N_("Mailing Lists"),
        "dolists": N_("lists.d.o"),
        "adolists": N_("lists.a.d.o"),
    },
    "files": {
        "label": N_("Files"),
        "people": N_("people.d.o"),
        "oldpeople": N_("oldpeople"),
    },
    "membership": {
        "label": N_("Membership"),
        "nm": N_("NM"),
        "dbfinger": N_("DB information via finger"),
        "db": N_("DB information via HTTP"),
        "salsa": N_("Salsa"),
        "wiki": N_("Wiki"),
        "forum": N_("Forum"),
    },
    "miscellaneous": {
        "label": N_("Miscellaneous"),
        "debtags": N_("debtags"),
        "planetname": N_("Planet Debian (name)"),
        "planetuser": N_("Planet Debian (username)"),
        "links": N_("links"),
        "website": N_("Debian website"),
        "search": N_("Debian search"),
        "gpgfinger": N_("OpenPGP public key via finger"),
        "gpgweb": N_("OpenPGP public key via HTTP"),
        "nm": N_("NM, AM participation"),
        "contrib": N_("Contribution information"),
        "repology": N_("Repology information"),
    },
    "ssh": {
        "label": N_("Information reachable via ssh (for Debian Members)"),
        "owndndoms": N_("owned debian.net domains"),
        "miainfo": N_(
            '<a href="https://wiki.debian.org/qa.debian.org/'
            'MIATeam">MIA</a> database information'
        ),
        "groupinfo": N_("Group membership information"),
    },
}

#: list of field name tuples for Debian Maintainers
DM_TUPLES = (("name", "name"), ("openpgpfp", "openpgpfp"), ("nonddemail", "email"))

#: list of field name tuples for Debian Developers
DD_TUPLES = (("username", "username"), ("salsausername", "username"))


def _get_label(section, url=None):
    if section in _LABELS:
        if url:
            if url in _LABELS[section]:
                return _LABELS[section][url]
        elif "label" in _LABELS[section]:
            return _LABELS[section]["label"]
    if url:
        return "%s.%s" % (section, url)
    return section


@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(LANGUAGES.keys())


@app.before_request
def before_request():
    g.locale = get_locale()


@app.route("/")
def index():
    form = DeveloperData()
    return render_template("showform.html", form=form)


@app.route("/result")
def urllist():
    form = DeveloperData(request.values)
    if form.validate():
        fields = dddatabuilder.build_data(form.data["email"])

        form_data = form.data.copy()

        if fields["type"] in (dddatabuilder.TYPE_DD, dddatabuilder.TYPE_DM):
            for dmtuple in DM_TUPLES:
                if not form_data[dmtuple[0]]:
                    form_data[dmtuple[0]] = fields[dmtuple[1]]
        if fields["type"] == dddatabuilder.TYPE_DD:
            for ddtuple in DD_TUPLES:
                if not form_data[ddtuple[0]]:
                    form_data[ddtuple[0]] = fields[ddtuple[1]]
        if not form_data["wikihomepage"] and form_data["name"]:
            log.debug("generate wikihomepage from name")
            form_data["wikihomepage"] = "".join(
                [part.capitalize() for part in form_data["name"].split()]
            )

        data = build_urls(form_data)

        if form_data["mode"] == "json":
            response = make_response(
                json.dumps(
                    dict(
                        [
                            ("{}.{}".format(entry[1], entry[2].name), entry[3])
                            for entry in data
                            if entry[0] == "url"
                        ]
                    )
                )
            )
            response.headers["Content-Type"] = "application/json"
            return response

        for entry in data:
            if entry[0] in ("url", "error"):
                entry.append(_get_label(entry[1], entry[2].name))
            elif entry[0] == "section":
                entry.append(_get_label(entry[1]))

        return render_template("showurls.html", urldata=data)
    return render_template("showform.html", form=form)


@app.route("/htmlformhelper.js")
def formhelper_js():
    response = make_response(render_template("showformscript.js"))
    response.headers["Content-Type"] = "text/javascript; charset=utf-8"
    return response


@app.route("/showformscripts/fetchdddata/")
def fetchdddata():
    form = DeveloperDataRequest(request.values)
    if form.validate():
        fields = dddatabuilder.build_data(form.data["email"])
        log.debug(fields)
        response = make_response(json.dumps(fields))
        response.headers["Content-Type"] = "application/json"
        return response
    abort(400, "\n".join(["%s: %s" % (key, form.errors[key]) for key in form.errors]))
