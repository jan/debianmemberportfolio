{# vim: ft=jinja
Helper JavaScript for the data input form.
Copyright © 2009-2022, Jan Dittberner <jan@dittberner.info>

This file is part of the Debian Member Portfolio service.

Debian Member Portfolio service is free software: you can redistribute it
and/or modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Debian Member Portfolio service is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero
General Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with this program.  If not, see <https://www.gnu.org/licenses/>.
#}
var defaulthiddendivs = new Array(
    '#namefield', '#openpgpfpfield', '#usernamefield', '#nonddemailfield',
    '#salsausernamefield', '#wikihomepagefield', '#forumsidfield');
var maskedfielddivs = new Array(
    '#namefield', '#openpgpfpfield', '#usernamefield', '#nonddemailfield',
    '#salsausernamefield', '#wikihomepagefield', '#forumsidfield');
var allfielddivs = new Array(
    '#namefield', '#openpgpfpfield', '#usernamefield', '#nonddemailfield',
    '#salsausernamefield', '#wikihomepagefield', '#forumsidfield');

function updateFields(data, textStatus) {
    if (data.type == 2) { // DD
        $('#name').prop('value', data.name).prop('readonly', 'readonly');
        $('#openpgpfp').prop('value', data.openpgpfp);
        $('#username').prop('value', data.username).prop(
            'readonly', 'readonly');
        $('#nonddemail').prop('value', data.email).focus();
        $('#salsausername').prop('value', data.username);
        $('#wikihomepage').prop('value', data.wikihomepage);

        $('#namefield').show();
        $('#openpgpfpfield').show();
        $('#usernamefield').show();
        $('#nonddemailfield').show();
        $('#salsausernamefield').show();
        $('#wikihomepagefield').show();
        $('#forumsidfield').show();

        $('#nonddemail').focus().select();
    } else if (data.type == 1) { // DM
        $('#name').prop('value', data.name).prop('readonly', 'readonly');
        $('#openpgpfp').prop('value', data.openpgpfp);
        $('#username').prop('value', '');
        $('#nonddemail').prop('value', data.email).focus();
        $('#wikihomepage').prop('value', data.wikihomepage);

        $('#namefield').show();
        $('#openpgpfpfield').show();
        $('#usernamefield').hide();
        $('#nonddemailfield').hide();
        $('#salsausernamefield').show();
        $('#wikihomepagefield').show();
        $('#forumsidfield').show();

        $('#salsausername').focus().select();
    } else {
        $('#nonddemail').prop('value', data.email);
        $('#name').removeAttr('readonly');
        $('#username').removeAttr('readonly').prop('value', '');
        $('#openpgpfp').prop('value', '');

        $('#usernamefield').hide();
        $('#openpgpfpfield').hide();
        $('#nonddemailfield').hide();
        $('#namefield').show();
        $('#salsausernamefield').show();
        $('#wikihomepagefield').show();
        $('#forumsidfield').show();

        $('#name').focus().select();
    }
}

function onChangeShowAll(event) {
    if ($('#showall').prop('checked')) {
        for (var fielddiv in allfielddivs) {
            $(allfielddivs[fielddiv]).show();
        }
    } else {
        for (var fielddiv in maskedfielddivs) {
            $(maskedfielddivs[fielddiv]).hide();
        }
    }
}

function onBlurEmail() {
    if ($.trim($('#email').prop('value')).length > 0) {
        $.ajax({
            'url' : '{{ url_for("fetchdddata") }}',
            'data' : {'email' : $('#email').prop('value')},
            'dataType' : 'json',
            'success' : updateFields,
            'error' : function(request, textStatus, errorThrown) {
                $('#email').focus();
            }
        });
    }
}

$(document).ready(function() {
    for (var index in defaulthiddendivs) {
        if (!$(defaulthiddendivs[index]).hasClass('witherrors')) {
            $(defaulthiddendivs[index]).hide();
        }
    }

    $('#showall').prop('checked', false).change(onChangeShowAll);
    $('#showallfield').show();
    $('#email').blur(onBlurEmail).focus();
});
