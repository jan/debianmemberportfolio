# -*- python -*-
# -*- coding: utf-8 -*-
#
# Debian Member Portfolio Service key finder module
#
# Copyright © 2009-2023 Jan Dittberner <jan@dittberner.info>
#
# This file is part of the Debian Member Portfolio Service.
#
# Debian Member Portfolio Service is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Debian Member Portfolio Service is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
This module provides tools for finding OpenPGP key information from a
given keyring.
"""

import logging
import sys
import time
from importlib import resources

db = None
cache_timestamp = 0


def _get_keyring_cache():
    global db, cache_timestamp
    if db is None or (time.time() - cache_timestamp) > 86300:
        import dbm
        import os.path

        dbm_filename = str(resources.files(__package__).joinpath("keyringcache.db"))
        logging.debug("reading cache data from %s", dbm_filename)
        assert os.path.exists(dbm_filename) and os.path.isfile(dbm_filename)
        db = dbm.open(dbm_filename[: -len(".db")], "r")
        cache_timestamp = time.time()
    return db


def _get_cached(cachekey):
    cache = _get_keyring_cache()
    logging.debug("cache lookup for %s", cachekey)
    if cachekey in cache:
        logging.debug("found entry %s", cache[cachekey])
        return cache[cachekey].decode("utf8")
    return None


def getFingerprintByEmail(email):
    """
    Gets the fingerprints associated with the given email address if
    available.
    """
    return _get_cached("fpr:email:%s" % email)


def getRealnameByEmail(email):
    """
    Gets the real names associated with the given email address if
    available.
    """
    return _get_cached("name:email:%s" % email)


def getLoginByEmail(email):
    """
    Gets the logins associated with the given email address if
    available.
    """
    return _get_cached("login:email:%s" % email)


def getLoginByFingerprint(fpr):
    """
    Gets the login associated with the given fingerprint if available.
    """
    return _get_cached("login:fpr:%s" % fpr)


def _dump_cache():
    cache = _get_keyring_cache()
    fprs = []
    for key in [key.decode("utf8") for key in list(cache.keys())]:
        if key.startswith("email:fpr:"):
            fpr = key.replace("email:fpr:", "")
            if not fpr in fprs:
                fprs.append(fpr)

    for fpr in fprs:
        login = getLoginByFingerprint(fpr)
        email = _get_cached("email:fpr:%s" % fpr)
        name = _get_cached("name:fpr:%s" % fpr)

        print(fpr, login, ":")
        print("   ", name, email)


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stderr, level=logging.WARNING)
    _dump_cache()
