# -*- python -*-
# -*- coding: utf-8 -*-
#
# Debian Member Portfolio Service data builder
#
# Copyright © 2009-2022 Jan Dittberner <jan@dittberner.info>
#
# This file is part of the Debian Member Portfolio Service.
#
# Debian Member Portfolio Service is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Debian Member Portfolio Service is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
This file contains code to build a representation of a person based on keyring
data associated to a given email address.
"""

import logging
from debianmemberportfolio.model import keyfinder

TYPE_NO = 0
TYPE_DM = 1
TYPE_DD = 2

log = logging.getLogger(__name__)


def build_data(email_address):
    """Build a DD data structure from a given email address."""
    fields = dict([(field, func(str(email_address)))
                   for (field, func) in
                   [('openpgpfp', keyfinder.getFingerprintByEmail),
                    ('name', keyfinder.getRealnameByEmail),
                    ('username', keyfinder.getLoginByEmail)]])
    fields['email'] = email_address
    if fields['username'] and fields['openpgpfp'] and fields['name']:
        fields['type'] = TYPE_DD
    elif fields['name'] and fields['openpgpfp']:
        fields['type'] = TYPE_DM
    else:
        fields['type'] = TYPE_NO
    if fields['name']:
        log.debug('generate wikihomepage from name')
        fields['wikihomepage'] = "".join(
            [part.capitalize() for part in fields['name'].split()])

    return fields
