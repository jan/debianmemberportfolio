alabaster==0.7.13 ; python_version >= "3.8" and python_version < "4.0"
babel==2.12.1 ; python_version >= "3.8" and python_version < "4.0"
cachecontrol[filecache]==0.12.11 ; python_version >= "3.8" and python_version < "4.0"
cachy==0.3.0 ; python_version >= "3.8" and python_version < "4.0"
certifi==2023.5.7 ; python_version >= "3.8" and python_version < "4.0"
cffi==1.15.1 ; python_version >= "3.8" and python_version < "4.0" and sys_platform == "linux"
charset-normalizer==3.1.0 ; python_version >= "3.8" and python_version < "4.0"
cleo==1.0.0a5 ; python_version >= "3.8" and python_version < "4.0"
colorama==0.4.6 ; python_version >= "3.8" and python_version < "4.0" and sys_platform == "win32"
crashtest==0.3.1 ; python_version >= "3.8" and python_version < "4.0"
cryptography==41.0.1 ; python_version >= "3.8" and python_version < "4.0" and sys_platform == "linux"
distlib==0.3.6 ; python_version >= "3.8" and python_version < "4.0"
docutils==0.17.1 ; python_version >= "3.8" and python_version < "4.0"
entrypoints==0.3 ; python_version >= "3.8" and python_version < "4.0"
filelock==3.12.0 ; python_version >= "3.8" and python_version < "4.0"
html5lib==1.1 ; python_version >= "3.8" and python_version < "4.0"
idna==3.4 ; python_version >= "3.8" and python_version < "4.0"
imagesize==1.4.1 ; python_version >= "3.8" and python_version < "4.0"
importlib-metadata==6.6.0 ; python_version >= "3.8" and python_version < "3.12"
importlib-resources==5.12.0 ; python_version >= "3.8" and python_version < "3.9"
jaraco-classes==3.2.3 ; python_version >= "3.8" and python_version < "4.0"
jeepney==0.8.0 ; python_version >= "3.8" and python_version < "4.0" and sys_platform == "linux"
jinja2==3.1.2 ; python_version >= "3.8" and python_version < "4.0"
keyring==23.13.1 ; python_version >= "3.8" and python_version < "4.0"
lockfile==0.12.2 ; python_version >= "3.8" and python_version < "4.0"
markupsafe==2.1.3 ; python_version >= "3.8" and python_version < "4.0"
more-itertools==9.1.0 ; python_version >= "3.8" and python_version < "4.0"
msgpack==1.0.5 ; python_version >= "3.8" and python_version < "4.0"
packaging==20.9 ; python_version >= "3.8" and python_version < "4.0"
pexpect==4.8.0 ; python_version >= "3.8" and python_version < "4.0"
pkginfo==1.9.6 ; python_version >= "3.8" and python_version < "4.0"
platformdirs==3.5.1 ; python_version >= "3.8" and python_version < "4.0"
poetry-babel-plugin==0.1.0 ; python_version >= "3.8" and python_version < "4.0"
poetry-core==1.6.1 ; python_version >= "3.8" and python_version < "4.0"
poetry==1.2.0b1 ; python_version >= "3.8" and python_version < "4.0"
ptyprocess==0.7.0 ; python_version >= "3.8" and python_version < "4.0"
pycparser==2.21 ; python_version >= "3.8" and python_version < "4.0" and sys_platform == "linux"
pygments==2.15.1 ; python_version >= "3.8" and python_version < "4.0"
pylev==1.4.0 ; python_version >= "3.8" and python_version < "4.0"
pyparsing==3.0.9 ; python_version >= "3.8" and python_version < "4.0"
pytz==2023.3 ; python_version >= "3.8" and python_version < "3.9"
pywin32-ctypes==0.2.0 ; python_version >= "3.8" and python_version < "4.0" and sys_platform == "win32"
requests-toolbelt==0.9.1 ; python_version >= "3.8" and python_version < "4.0"
requests==2.31.0 ; python_version >= "3.8" and python_version < "4.0"
secretstorage==3.3.3 ; python_version >= "3.8" and python_version < "4.0" and sys_platform == "linux"
shellingham==1.5.0.post1 ; python_version >= "3.8" and python_version < "4.0"
six==1.16.0 ; python_version >= "3.8" and python_version < "4.0"
snowballstemmer==2.2.0 ; python_version >= "3.8" and python_version < "4.0"
sphinx==5.1.1 ; python_version >= "3.8" and python_version < "4.0"
sphinxcontrib-applehelp==1.0.4 ; python_version >= "3.8" and python_version < "4.0"
sphinxcontrib-devhelp==1.0.2 ; python_version >= "3.8" and python_version < "4.0"
sphinxcontrib-htmlhelp==2.0.1 ; python_version >= "3.8" and python_version < "4.0"
sphinxcontrib-jsmath==1.0.1 ; python_version >= "3.8" and python_version < "4.0"
sphinxcontrib-qthelp==1.0.3 ; python_version >= "3.8" and python_version < "4.0"
sphinxcontrib-serializinghtml==1.1.5 ; python_version >= "3.8" and python_version < "4.0"
tomlkit==0.11.8 ; python_version >= "3.8" and python_version < "4.0"
urllib3==1.26.16 ; python_version >= "3.8" and python_version < "4.0"
virtualenv==20.23.0 ; python_version >= "3.8" and python_version < "4.0"
webencodings==0.5.1 ; python_version >= "3.8" and python_version < "4.0"
zipp==3.15.0 ; python_version >= "3.8" and python_version < "3.12"
