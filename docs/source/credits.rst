Credits
=======

The Debian Member Portfolio Service contains contributions from several people.

Code
----

  * Jan Dittberner <jandd at debian dot org>
  * Paul Wise <pabs at debian dot org>
  * Olivier Berger <olivier.berger at telecom-sudparis dot eu>
  * Juri Grabowski <git-commit at jugra dot de>

Translations
------------

  * Adolfo Jayme Barrientos (Spanish, French, Portuguese (Brazil))
  * Allan Nordhøy (Norwegian Bokmål)
  * Boyuan Yang (Chinese (simplified))
  * ButterflyOfFire (French, Portuguese (Brazil))
  * Daniel Manzano (Portugues (Brazil))
  * Frans Spiesschaert (Dutch)
  * Heimen Stoffels (Dutch)
  * Izharul Haq (Indonesian)
  * Jan Dittberner (German, English)
  * Manuela Silva (Portugues (Portugal))
  * Mattias Münster (Swedish)
  * Michal Biesiada (Polish)
  * Nikita Epifanov (Russian)
  * Olivier Humbert (French)
  * Petter Reinholdtsen (Norwegian Bokmål)
  * Prachi Joshi (Marathi)
  * Reza Almanda (Indonesian)
  * Shuji Sado (Japanese)
  * Stéphane Aulery (French)
  * Tao Wang (Chinese (simplified))
  * Thanos Siourdakis (Greek)
  * Tiago Naufragado (Portugues (Brazil))
  * Wellington Terumi Uemura (Portugues (Brazil))
  * chimez (Chinese (China))
  * ssantos (Portugues (Portugal))

If you think your name is missing please tell me (Jan Dittberner) about your
contribution and I'll add you.

