# Debian Member Portfolio Service

This is a service implementation that returns a set of personalized URLs as outlined in
https://wiki.debian.org/DDPortfolio. It takes the Debian Member's full name and email address as input and returns
a JSON formatted array or an HTML page of URLs.

See https://debian-member-portfolio-service.readthedocs.org/ for more documentation (or its source in
docs/source/devdocs.rst), including how to configure a development environment.

## Translations

Translations for the Debian Member Portfolio service are maintained using
[Weblate](https://hosted.weblate.org/projects/debian-member-portfolio-service/translations/). Thanks to Weblate for
hosting the translation service and to all contributors of translations.
